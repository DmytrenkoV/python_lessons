
input_data = {
    "Student_1": {"score": 78, "status": "Failed"},
    "Student_2": {"score": 82, "status": "Passed"},
    "Student_3": {"score": 97, "status": "Passed"},
    "Student_4": {"score": 86, "status": "Passed"},
    "Student_5": {"score": 67, "status": "Passed"},
    "Student_6": {"score": 75, "status": "Passed"},
}

# input_data = {
#     "Student_1": {"score": 84, "status": "Passed"},
#     "Student_2": {"score": 78, "status": "Passed"},
#     "Student_3": {"score": 65, "status": "Failed"},
#     "Student_4": {"score": 90, "status": "Passed"},
#     "Student_5": {"score": 72, "status": "Failed"},
# }

# input_data = {
#     "Student_1": {"score": 78, "status": "Failed"},
#     "Student_6": {"score": 75, "status": "Passed"},
# }

# input_data = {
#     "Student_1": {"score": 100, "status": "Failed"},
#     "Student_3": {"score": 97, "status": "Passed"},
#     "Student_6": {"score": 75, "status": "Passed"},
# }

# input_data = {
#     "Student_6": {"score": 75, "status": "Passed"},
# }


from pprint import pprint

def check_professor(data):
    data = tuple(tuple(grade.values()) for student_name, grade in data.items())
    pprint(data)

    failed_scores = []
    passed_scores = []

    for score, status in data:
        if status == "Failed":
            failed_scores.append(score)
        elif status == "Passed":
            passed_scores.append(score)


    min_passed_score = min(passed_scores)
    max_failed_score = max(failed_scores) if failed_scores else None

    if max_failed_score is not None and max_failed_score >= min_passed_score:
        print("Профессор непослідовний")
        return

    passing_range = (max_failed_score + 1 if max_failed_score is not None else min_passed_score, min_passed_score)

    print(f"Професор послідовний, а поріг складання іспиту знаходиться в діапазоні {passing_range[0]} – {passing_range[1]} балів")


    for student_name, grade in input_data.items():
        score = grade["score"]
        if score >= passing_range[0]:
            range_category = "високий діапазон"
        else:
            range_category = "низький діапазон"
        print(f"{student_name}: оцінка {score} - {range_category}")

check_professor(input_data)
