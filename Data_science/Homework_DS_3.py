import pandas as pd


url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
column_names = ["sepal_length", "sepal_width", "petal_length", "petal_width", "class"]
iris_df = pd.read_csv(url, header=None, names=column_names)

# --- Завдання 1: Аналіз даних Iris ---

mean_sepal_length = iris_df.groupby('class')['sepal_length'].mean()
print("Середня довжина чашолистика (sepal length) для кожного виду ірису:")
print(mean_sepal_length)
print()

max_petal_width_setosa = iris_df[iris_df['class'] == 'Iris-setosa']['petal_width'].max()
print("Максимальна ширина листка (petal width) для виду 'setosa':")
print(max_petal_width_setosa)
print()

petal_length_distribution = iris_df['petal_length'].describe()
print("Розподіленість довжини листка (petal length) для всіх ірисів:")
print(petal_length_distribution)
print()

# --- Завдання 2: Фільтрація та відбір даних ---

versicolor_df = iris_df[iris_df['class'] == 'Iris-versicolor']
print("Дані для ірисів виду 'versicolor':")
print(versicolor_df.head())
print()

filtered_df = iris_df[iris_df['petal_length'] > 5.0]
print("Іриси з довжиною листка більше 5.0:")
print(filtered_df.head())
print()

# --- Завдання 3: Групування та агрегація ---

mean_petal_width = iris_df.groupby('class')['petal_width'].mean()
print("Середня ширина листка (petal width) для кожного виду ірису:")
print(mean_petal_width)
print()

min_sepal_length = iris_df.groupby('class')['sepal_length'].min()
print("Мінімальна довжина чашолистика (sepal length) для кожного виду ірису:")
print(min_sepal_length)
print()

mean_petal_length = iris_df['petal_length'].mean()
count_above_average = iris_df[iris_df['petal_length'] > mean_petal_length].groupby('class').size()
print("Кількість ірисів кожного виду з довжиною листка більше за середню довжину листка всіх ірисів:")
print(count_above_average)
