
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
import sys

data = sns.load_dataset('titanic')

data = data.drop(['deck', 'embark_town', 'alive'], axis=1)

data['age'] = data['age'].fillna(data['age'].median())
data['embarked'] = data['embarked'].fillna(data['embarked'].mode()[0])


data = pd.get_dummies(data, columns=['sex', 'embarked'], drop_first=True)

data = data.drop(['who', 'adult_male', 'class'], axis=1)

sns.countplot(x='survived', data=data)
plt.title('Выживаемость пассажиров')
plt.show()

data['FamilySize'] = data['sibsp'] + data['parch'] + 1
data['IsAlone'] = (data['FamilySize'] == 1).astype(int)

scaler = StandardScaler()
data[['age', 'fare']] = scaler.fit_transform(data[['age', 'fare']])


X = data.drop('survived', axis=1)
y = data['survived']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


model = LogisticRegression()
model.fit(X_train, y_train)

y_pred = model.predict(X_test)
print('Accuracy:', accuracy_score(y_test, y_pred))
print('Precision:', precision_score(y_test, y_pred))
print('Recall:', recall_score(y_test, y_pred))
print('F1 Score:', f1_score(y_test, y_pred))

conf_matrix = confusion_matrix(y_test, y_pred)
sns.heatmap(conf_matrix, annot=True, cmap='Blues')
plt.title('Confusion Matrix')
plt.show()

param_grid = {'C': [0.1, 1, 10], 'kernel': ['linear', 'rbf']}
grid = GridSearchCV(SVC(), param_grid, refit=True, verbose=2)
grid.fit(X_train, y_train)

best_model = grid.best_estimator_

y_pred_best = best_model.predict(X_test)
print('Best Model Accuracy:', accuracy_score(y_test, y_pred_best))
print('Best Model Precision:', precision_score(y_test, y_pred_best))
print('Best Model Recall:', recall_score(y_test, y_pred_best))
print('Best Model F1 Score:', f1_score(y_test, y_pred_best))

conf_matrix_best = confusion_matrix(y_test, y_pred_best)
sns.heatmap(conf_matrix_best, annot=True, cmap='Blues')
plt.title('Confusion Matrix - Best Model')
plt.show()
sys.exit()