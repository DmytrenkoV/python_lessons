from flask import Flask, request, jsonify
import json

app = Flask(__name__)

questions_data = [
    {
        "id": 1,
        "question": "Какой химический элемент обозначается символом 'O'?",
        "options": ["Водород", "Кислород", "Углерод", "Азот"],
        "correct_option": 1
    },
    {
        "id": 2,
        "question": "Какая планета ближе всего к Солнцу?",
        "options": ["Земля", "Венера", "Марс", "Меркурий"],
        "correct_option": 3
    },
]

@app.route('/get_question/<int:question_id>', methods=['GET'])
def get_question(question_id):
    question = next((q for q in questions_data if q["id"] == question_id), None)
    if question:
        return jsonify(question)
    else:
        return jsonify({"error": "Вопрос не найден"}), 404


@app.route('/check_answer/<int:question_id>', methods=['POST'])
def check_answer(question_id):
    user_answer = request.json.get('answer')
    question = next((q for q in questions_data if q["id"] == question_id), None)

    if not question:
        return jsonify({"error": "Вопрос не найден"}), 404

    if user_answer is None:
        return jsonify({"error": "Ответ не предоставлен"}), 400

    is_correct = question["correct_option"] == user_answer
    return jsonify({"correct": is_correct})


if __name__ == '__main__':
    app.run(debug=True)
