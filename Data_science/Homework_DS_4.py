import seaborn as sns
import matplotlib.pyplot as plt

penguins = sns.load_dataset('penguins')

print(penguins.head())

sns.scatterplot(x='flipper_length_mm', y='body_mass_g', data=penguins, hue='species')
plt.title('Зависимость веса пингвинов от длины их крыльев')
plt.xlabel('Длина крыла (мм)')
plt.ylabel('Вес (г)')
plt.show()

sns.boxplot(x='species', y='flipper_length_mm', data=penguins)
plt.title('Размеры крыльев у разных видов пингвинов')
plt.xlabel('Вид пингвина')
plt.ylabel('Длина крыла (мм)')
plt.show()

penguins = sns.load_dataset('penguins')

penguins_cleaned = penguins.dropna()

penguins_numeric = penguins_cleaned.select_dtypes(include='number')

corr = penguins_numeric.corr()
sns.heatmap(corr, annot=True, cmap='coolwarm')
plt.title('Тепловая карта корреляции характеристик пингвинов')
plt.show()
