import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier

iris_df = pd.read_csv('iris.csv', encoding='latin1', sep=';')

print("Форма данных:", iris_df.shape)
print("Типы данных:", iris_df.dtypes)
print("Первые 3 строки данных:\n", iris_df.head(3))

print("\nОсновные статистические данные:\n", iris_df.describe())

iris_df.hist(figsize=(10, 8))
plt.suptitle("Общая статистика данных")
plt.show()

iris_df['employment_region'].value_counts().plot(kind='bar', color='green')
plt.title("Частота значений в столбце 'employment_region'")
plt.xlabel("Регион занятости")
plt.ylabel("Количество")
plt.show()

X = iris_df.select_dtypes(include='number')  # Выбираем все числовые атрибуты
y = iris_df['employment_region']  # Используем 'employment_region' в качестве меток

label_encoder = LabelEncoder()
y_encoded = label_encoder.fit_transform(y)

X_train, X_test, y_train, y_test = train_test_split(X, y_encoded, test_size=0.3, random_state=42)
print("\nРазмер тренировочных данных X:", X_train.shape)
print("Размер тестовых данных X:", X_test.shape)

knn = KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)

y_pred = knn.predict(X_test)

print("\nПредсказанные значения для тестового набора:\n", y_pred)
print("\nФактические значения для тестового набора:\n", y_test)

