import numpy as np
from PIL import Image
# -------------------------- 1 ----------------------
array1 = np.random.rand(5, 5)
array2 = np.random.rand(5, 5)

sum_array1 = np.sum(array1)
sum_array2 = np.sum(array2)

difference = array1 - array2

product = array1 * array2

power = np.power(array1, array2)

print("Массив 1:\n", array1)
print("Массив 2:\n", array2)
print("Сумма элементов массива 1:", sum_array1)
print("Сумма элементов массива 2:", sum_array2)
print("Разность между массивами:\n", difference)
print("Перемножение массивов:\n", product)
print("Возведение в степень:\n", power)

# ----------------------------2----------------------------
chessboard = np.zeros((8, 8), dtype=int)
chessboard[1::2, ::2] = 1
chessboard[::2, 1::2] = 1

row_3 = chessboard[2, :]

column_5 = chessboard[:, 4]

subarray_3x3 = chessboard[:3, :3]

print("\nШахматная доска:\n", chessboard)
print("3-я строка:\n", row_3)
print("5-й столбец:\n", column_5)
print("Подмассив 3x3:\n", subarray_3x3)

# ---------------------------3--------------------------------
image = Image.open('RGB.webp')
image_array = np.array(image)


print("\nРазмер изображения:", image_array.shape)
print("Тип данных:", image_array.dtype)


mean_values = np.mean(image_array, axis=(0, 1))
min_values = np.min(image_array, axis=(0, 1))
max_values = np.max(image_array, axis=(0, 1))

print("Средние значения по каналам (R, G, B):", mean_values)
print("Минимальные значения по каналам (R, G, B):", min_values)
print("Максимальные значения по каналам (R, G, B):", max_values)


total_intensity = np.sum(image_array)
print("Общая сумма интенсивности пикселей:", total_intensity)


normalized_image = image_array / max_values
normalized_image = (normalized_image * 255).astype(np.uint8)


Image.fromarray(normalized_image).save('normalized_image.jpg')
print("Нормализованное изображение сохранено как 'normalized_image.jpg'")
