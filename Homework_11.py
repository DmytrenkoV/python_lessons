import time
import math

def naive_prime_search(n):
    primes = []
    for num in range(2, n + 1):
        is_prime = True
        for i in range(2, int(num ** 0.5) + 1):
            if num % i == 0:
                is_prime = False
                break
        if is_prime:
            primes.append(num)
    return primes


def sieve_of_eratosthenes(n):
    A = [True] * (n + 1)
    for i in range(2, int(math.sqrt(n)) + 1):
        if A[i]:
            for j in range(i * i, n + 1, i):
                A[j] = False
    primes = [i for i in range(2, n + 1) if A[i]]
    return primes


data_to_test = [100, 1_000, 10_000, 100_000]


def test_method(data):
    final_data = []
    for number in data:
        start_time = time.time()
        naive_prime_search(number)
        naive_time = time.time() - start_time

        start_time = time.time()
        sieve_of_eratosthenes(number)
        sieve_time = time.time() - start_time

        final_data.append((number, naive_time, sieve_time))

    for number, naive_time, sieve_time in final_data:
        print(
            f"list_range={number}, naive_method_time={naive_time:.6f} seconds, sieve_method_time={sieve_time:.6f} seconds")


test_method(data_to_test)
