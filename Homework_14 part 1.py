class Product:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

class Order:
    def __init__(self, products):
        self.products = [Product(product) for product in products if product]

    def __str__(self):
        return '\n'.join(str(product) for product in self.products)

class OrderProcessor:
    def __init__(self, filename):
        self.filename = filename
        self.orders = []

    def read_file(self):
        with open(self.filename, 'r', encoding='utf-8') as file:
            data = file.read()
        return data

    def parse_data(self, data):
        lines = data.split('\n')
        for line in lines:
            products = line.split('@@@')
            order = Order(products)
            self.orders.append(order)

    def process_orders(self):
        data = self.read_file()
        self.parse_data(data)

    def display_orders(self):
        for i, order in enumerate(self.orders):
            print(f"Заказ {i + 1}:")
            print(order)
            print()

# Использование классов
processor = OrderProcessor('orders.txt')
processor.process_orders()
processor.display_orders()

