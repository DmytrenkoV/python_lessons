print("Point #1")
# Запрашиваем у пользователя целое число
number = int(input("Введите целое число: "))

# Проверяем условия и сохраняем результаты в переменные
is_divisible_by_3 = (number % 3 == 0)
is_divisible_by_5 = (number % 5 == 0)

# Используем результаты проверок для вывода соответствующих сообщений
if is_divisible_by_3 and is_divisible_by_5:
    print("ham")
elif is_divisible_by_3:
    print("foo")
elif is_divisible_by_5:
    print("bar")


print("Point #2")

# Запрашиваем у пользователя два числа
number1 = float(input("Введите первое число: "))
number2 = float(input("Введите второе число: "))

# Проверяем условия и сохраняем результаты в переменные
a = (number1 > number2)
b = (number1 < number2)

# Используем результаты проверок для вывода соответствующих сообщений
if b:
    print(f"Меньшее число: {number1}")
    print(f"Большее число: {number2}")
elif a:
    print(f"Меньшее число: {number2}")
    print(f"Большее число: {number1}")
else:
    print()


print("Point #3")

# Запрашиваем у пользователя три числа
number1 = float(input("Введите первое число: "))
number2 = float(input("Введите второе число: "))
number3 = float(input("Введите третье число: "))

# Создаем список из введенных чисел
numbers = [number1, number2, number3]

# Сортируем список
numbers.sort()

# Выводим числа в порядке возрастания
print(f"Наименьшее число: {numbers[0]}")
print(f"Среднее число: {numbers[1]}")
print(f"Наибольшее число: {numbers[2]}")

print("Point #4")

# Проходим по всем числам от 1 до 100
for number in range(1, 101):
    # Проверяем условия и сохраняем результаты в переменные
    is_divisible_by_3 = (number % 3 == 0)
    is_divisible_by_5 = (number % 5 == 0)

    # Используем результаты проверок для вывода соответствующих сообщений
    if is_divisible_by_3 and is_divisible_by_5:
        print("fizz buzz")
    elif is_divisible_by_3:
        print("fizz")
    elif is_divisible_by_5:
        print("buzz")

print("Point #5")

# Проходим по всем числам от 1 до 100
for number in range(1, 101):
    # Проверяем, делится ли число на 7 или содержит цифру 7
    if number % 7 == 0 or '7' in str(number):
        print("BOOM")
    else:
        print(number)
