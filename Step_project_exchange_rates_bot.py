import telebot
from telebot import types
import requests
import json
import time

API_TOKEN = '6931772836:AAGlWtnNZ9fC-Rcrs4HLFqffcmXPpdyjAEg'
REAL_API_URL = 'https://api.monobank.ua/bank/currency'

bot = telebot.TeleBot(API_TOKEN)
conversion_history = []
user_data = {}

cached_rates = None
last_api_call_time = 0
CACHE_TIMEOUT = 300

def get_exchange_rate(currency_code_a, currency_code_b):
    global cached_rates, last_api_call_time

    current_time = time.time()
    if cached_rates is None or (current_time - last_api_call_time) > CACHE_TIMEOUT:
        try:
            response = requests.get(REAL_API_URL)
            response.raise_for_status()
            cached_rates = response.json()
            last_api_call_time = current_time
        except requests.exceptions.RequestException as e:
            print(f"Error fetching rates: {e}")
            return None

    for rate in cached_rates:
        if rate['currencyCodeA'] == currency_code_a and rate['currencyCodeB'] == currency_code_b:
            return rate.get('rateBuy') or rate.get('rateCross')

    for rate in cached_rates:
        if rate['currencyCodeA'] == currency_code_b and rate['currencyCodeB'] == currency_code_a:
            inverse_rate = rate.get('rateSell')
            if inverse_rate:
                return 1 / inverse_rate

    print(f"Exchange rate not found for {currency_code_a} to {currency_code_b}")
    return None

def currency_keyboard():
    markup = types.ReplyKeyboardMarkup(row_width=3, resize_keyboard=True)
    markup.add('USD', 'UAH', 'EUR')
    return markup

@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, "Привет! Я бот для конвертации валют. Выберите исходную валюту.", reply_markup=currency_keyboard())

@bot.message_handler(func=lambda message: message.text in ['USD', 'UAH', 'EUR'])
def handle_currency_from(message):
    user_data[message.chat.id] = {'currency_from': message.text}
    bot.reply_to(message, f"Вы выбрали {message.text} как исходную валюту. Теперь введите сумму для конвертации.")
    bot.register_next_step_handler(message, handle_amount)

def handle_amount(message):
    try:
        amount = float(message.text)
        if amount <= 0:
            bot.reply_to(message, "Сумма должна быть положительным числом.")
            return

        user_data[message.chat.id]['amount'] = amount
        bot.reply_to(message, "Выберите целевую валюту.", reply_markup=currency_keyboard())
        bot.register_next_step_handler(message, handle_currency_to)

    except ValueError:
        bot.reply_to(message, "Ошибка: введите корректную сумму.")

def handle_currency_to(message):
    currency_to = message.text
    user_id = message.chat.id
    currency_from = user_data[user_id]['currency_from']
    amount = user_data[user_id]['amount']

    currency_codes = {
        "USD": 840,
        "UAH": 980,
        "EUR": 978
    }

    if currency_from not in currency_codes or currency_to not in currency_codes:
        bot.reply_to(message, "Неверная валюта. Пожалуйста, используйте USD, UAH или EUR.")
        return

    rate = get_exchange_rate(currency_codes[currency_from], currency_codes[currency_to])
    if rate is None:
        bot.reply_to(message, f"Не удалось найти курс обмена между {currency_from} и {currency_to}. Попробуйте позже.")
        return

    converted_amount = amount * rate
    bot.reply_to(message, f"{amount} {currency_from} = {converted_amount:.2f} {currency_to}")

    conversion_history.append({
        'amount': amount,
        'currency_from': currency_from,
        'currency_to': currency_to,
        'converted_amount': converted_amount
    })

    with open('history.json', 'w') as f:
        json.dump(conversion_history, f)

    user_data.pop(user_id)

bot.polling()
