#1 point
print("Завдання №1")

# Запрашиваем у пользователя имя и месячную зарплату
name = input("Введите ваше имя: ")
monthly_salary = float(input("Введите вашу месячную зарплату в долларах: "))
# print(monthly_salary)
# Рассчитываем годовую зарплату
annual_salary = monthly_salary * 12
# print(annual_salary)

# Преобразуем годовую зарплату в тысячи долларов
annual_salary_thousands = annual_salary / 1000
# print(annual_salary_thousands)
# Выводим результат
print(f"Річна зарплата {name} складає {annual_salary_thousands} тис. доларів")

# 2 point

print("Завдання №2")

# Запрашиваем у пользователя целое число
number = int(input("Введите целое число: "))

# Проверяем, что число в диапазоне от 100 до 999 и является четным
is_valid = 100 <= number <= 999 and number % 2 == 0

# Выводим результат
print(is_valid)

#3 point

print("Завдання №3")

# Запрашиваем у пользователя целое число
number = input("Введите целое число от 101 до 999: ")
print(number)
# Преобразуем число в строку, затем переворачиваем строку и снова преобразуем в число
reversed_number = number[::-1]

# Выводим результат
print(reversed_number)

#4 point

print("Завдання №4")

# Запрашиваем у пользователя два целых числа
number1 = int(input("Введите первое целое число: "))
number2 = int(input("Введите второе целое число: "))

# Вычисляем сумму
sum_result = number1 + number2

# Вычисляем разницу
difference_result = number1 - number2

# Вычисляем результат умножения
multiplication_result = number1 * number2

# Вычисляем результат деления
division_result = number1 / number2

# Вычисляем остаток от деления
remainder_result = number1 % number2

# Проверяем, больше ли или равно первое число второму
comparison_result = number1 >= number2

# Выводим результаты
print("Сумма:", sum_result)
print("Разница:", difference_result)
print("Произведение:", multiplication_result)
print("Деление:", division_result)
print("Остаток от деления:", remainder_result)
print("Первое число больше или равно второму:", comparison_result)