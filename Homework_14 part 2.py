import requests

API_KEY = "e90fab7014064d2c88795d9fd95afa6f"


class OpenWeatherMap:
    def __init__(self, city=None, lat=None, lon=None):
        self.api_key = API_KEY
        self.city = city
        self.lat = lat
        self.lon = lon
        self.data = None

        if city:
            self.get_weather_by_city()
        elif lat and lon:
            self.get_weather_by_coordinates()
        else:
            raise ValueError("Необходимо указать либо город, либо координаты")

    def get_weather_by_city(self):
        url = f"http://api.openweathermap.org/data/2.5/weather?q={self.city}&appid={self.api_key}&units=metric"
        self.data = requests.get(url).json()

    def get_weather_by_coordinates(self):
        url = f"http://api.openweathermap.org/data/2.5/weather?lat={self.lat}&lon={self.lon}&appid={self.api_key}&units=metric"
        self.data = requests.get(url).json()

    def get_temp(self):
        return self.data.get('main', {}).get('temp')

    def get_weather(self):
        return self.data.get('weather', [{}])[0].get('description')

    def get_wind(self):
        return self.data.get('wind', {}).get('speed')

    def get_city(self):
        return self.data.get('name')

    def get_text(self):
        return f"Город: {self.get_city()}\nТемпература: {self.get_temp()}°C\nПогода: {self.get_weather()}\nСкорость ветра: {self.get_wind()} м/с"

    def get_any_key(self, *keys):
        return {key: self.data.get(key) for key in keys}

    def __str__(self):
        return self.get_text()


def main():
    city = input("Введите название города: ")
    weather = OpenWeatherMap(city=city)
    print(weather)


    keys = input("Введите ключи для получения значений (через запятую): ").split(',')
    key_values = weather.get_any_key(*keys)
    for key, value in key_values.items():
        print(f"{key}: {value}")


if __name__ == "__main__":
    main()
