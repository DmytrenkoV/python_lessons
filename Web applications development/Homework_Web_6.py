import psycopg2
from psycopg2 import sql


class User:
    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

    @staticmethod
    def initialize_db():
        try:
            connection = psycopg2.connect(
                dbname="mydb", user="vladyslav", password="4364", host="localhost", port="5532"
            )
            cursor = connection.cursor()

            cursor.execute("""
                CREATE TABLE IF NOT EXISTS users (
                    id SERIAL PRIMARY KEY,
                    username TEXT UNIQUE,
                    password TEXT,
                    email TEXT UNIQUE
                )
            """)

            cursor.execute("""
                CREATE TABLE IF NOT EXISTS site_registrations (
                    id SERIAL PRIMARY KEY,
                    user_id INTEGER REFERENCES users(id),
                    site_name TEXT,
                    login TEXT,
                    password TEXT,
                    auth_type TEXT
                )
            """)

            connection.commit()
            cursor.close()
            connection.close()
            print("Database initialized.")
        except psycopg2.Error as e:
            print(f"Database initialization error: {e}")

    def register(self):
        try:
            connection = psycopg2.connect(
                dbname="mydb", user="vladyslav", password="4364", host="localhost", port="5532"
            )
            cursor = connection.cursor()

            cursor.execute("SELECT username, email FROM users WHERE username = %s OR email = %s",
                           (self.username, self.email))
            duplicate = cursor.fetchone()
            if duplicate:
                print("Error: Username or email already exists.")
                return

            cursor.execute("""
                INSERT INTO users (username, password, email)
                VALUES (%s, %s, %s)
            """, (self.username, self.password, self.email))

            connection.commit()
            cursor.close()
            connection.close()
            print("User successfully registered!")
        except psycopg2.Error as e:
            print(f"Registration error: {e}")

    @staticmethod
    def login(username, password):
        try:
            connection = psycopg2.connect(
                dbname="mydb", user="vladyslav", password="4364", host="localhost", port="5532"
            )
            cursor = connection.cursor()
            cursor.execute("""
                SELECT * FROM users WHERE username = %s AND password = %s
            """, (username, password))
            user = cursor.fetchone()
            cursor.close()
            connection.close()
            return user is not None
        except psycopg2.Error as e:
            print(f"Login error: {e}")
            return False


class SiteRegistration:
    def __init__(self, user_id, site_name, login, password, auth_type):
        self.user_id = user_id
        self.site_name = site_name
        self.login = login
        self.password = password
        self.auth_type = auth_type

    def add_registration(self):
        try:
            connection = psycopg2.connect(
                dbname="mydb", user="vladyslav", password="4364", host="localhost", port="5532"
            )
            cursor = connection.cursor()

            cursor.execute("""
                INSERT INTO site_registrations (user_id, site_name, login, password, auth_type)
                VALUES (%s, %s, %s, %s, %s)
            """, (self.user_id, self.site_name, self.login, self.password, self.auth_type))

            connection.commit()
            cursor.close()
            connection.close()
            print("Site registration added.")
        except psycopg2.Error as e:
            print(f"Error adding site registration: {e}")

    @staticmethod
    def get_registrations_by_user(user_id):
        try:
            connection = psycopg2.connect(
                dbname="mydb", user="vladyslav", password="4364", host="localhost", port="5532"
            )
            cursor = connection.cursor()
            cursor.execute("""
                SELECT site_name, login, auth_type FROM site_registrations WHERE user_id = %s
            """, (user_id,))
            registrations = cursor.fetchall()
            cursor.close()
            connection.close()

            if registrations:
                print("Site registrations:")
                for reg in registrations:
                    print(f"Site: {reg[0]}, Login: {reg[1]}, Auth Type: {reg[2]}")
            else:
                print("No registrations found for this user.")
        except psycopg2.Error as e:
            print(f"Error retrieving registrations: {e}")


User.initialize_db()

while True:
    print("\nChoose an option:")
    print("1 - Register")
    print("2 - Login")
    print("3 - Add Site Registration")
    print("4 - View My Registrations")
    print("5 - Exit")

    choice = input("Your choice: ")

    if choice == "1":
        username = input("Enter username: ")
        password = input("Enter password: ")
        email = input("Enter email: ")
        user = User(username, password, email)
        user.register()

    elif choice == "2":
        username = input("Enter username: ")
        password = input("Enter password: ")
        if User.login(username, password):
            print("Login successful!")
        else:
            print("Invalid credentials!")

    elif choice == "3":
        user_id = int(input("Enter your user ID: "))
        site_name = input("Enter site name: ")
        auth_type = input("Enter authentication type (Google, Apple, Facebook, Other): ")
        if auth_type.lower() in ["google", "apple", "facebook"]:
            login, password = None, None
        else:
            login = input("Enter login: ")
            password = input("Enter password: ")
        registration = SiteRegistration(user_id, site_name, login, password, auth_type)
        registration.add_registration()

    elif choice == "4":
        user_id = int(input("Enter your user ID: "))
        SiteRegistration.get_registrations_by_user(user_id)

    elif choice == "5":
        print("Goodbye!")
        break

    else:
        print("Invalid choice. Try again.")
