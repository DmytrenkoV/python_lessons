from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from decouple import config
from .forms import UserRegisterForm
from .models import SearchHistory
import requests

API_KEY = config('API_KEY')

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home')
    else:
        form = UserRegisterForm()
    return render(request, 'register.html', {'form': form})

def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('home')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

@login_required
def home(request):
    weather = None
    if request.method == 'POST':
        city = request.POST.get('city')
        url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={api_key}&units=metric"


        response = requests.get(url).json()


        if response.get('cod') == 200:
            weather = {
                'city': city,
                'temperature': response['main']['temp'],
                'description': response['weather'][0]['description'],
            }
            SearchHistory.objects.create(
                user=request.user,
                city=city,
                temperature=response['main']['temp'],
                description=response['weather'][0]['description']
            )
    return render(request, 'main.html', {'weather': weather})


@login_required
def history(request):
    searches = SearchHistory.objects.filter(user=request.user).order_by('-search_date')
    return render(request, 'history.html', {'searches': searches})

