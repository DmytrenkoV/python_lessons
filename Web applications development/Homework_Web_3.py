import math
import time
from concurrent.futures import ThreadPoolExecutor

def is_prime(n):
    if n <= 1:
        return False
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

def find_primes_single_thread(start, end):
    primes = []
    for number in range(start, end + 1):
        if is_prime(number):
            primes.append(number)
    return primes

def find_primes_multi_thread(start, end):
    mid = (start + end) // 2
    results = []

    with ThreadPoolExecutor(max_workers=2) as executor:
        future1 = executor.submit(find_primes_single_thread, start, mid)
        future2 = executor.submit(find_primes_single_thread, mid + 1, end)

        results.extend(future1.result())
        results.extend(future2.result())

    return sorted(results)

def test_functions(start, end):
    start_time = time.time()
    single_thread_result = find_primes_single_thread(start, end)
    single_thread_duration = time.time() - start_time
    print(f"Single-threaded result: {single_thread_result}")
    print(f"Single-threaded duration: {single_thread_duration:.4f} seconds")

    start_time = time.time()
    multi_thread_result = find_primes_multi_thread(start, end)
    multi_thread_duration = time.time() - start_time
    print(f"Multi-threaded result: {multi_thread_result}")
    print(f"Multi-threaded duration: {multi_thread_duration:.4f} seconds")

    assert single_thread_result == multi_thread_result, "Results are different!"
    print("Both methods returned the same result.")

    if multi_thread_duration > 0:
        print(f"Speed-up factor: {single_thread_duration / multi_thread_duration:.2f}")
    else:
        print("Multi-threaded duration is too short to measure a speed-up factor accurately.")


test_functions(10, 10000)
