import sys

def check_division_error(func):
    def wrapper(a, b):
        try:
            return func(a, b)
        except ZeroDivisionError:
            print("Ошибка: деление на ноль!")
            sys.exit(1)
    return wrapper

def check_index_error(func):
    def wrapper(lst, idx):
        try:
            return func(lst, idx)
        except IndexError:
            print("Ошибка: индекс выходит за границы списка!")
            sys.exit(1)
    return wrapper

@check_division_error
def divide(a, b):
    return a / b

@check_index_error
def get_element(lst, idx):
    return lst[idx]

print("Тестирование функции divide:")
try:
    print(divide(10, 2))
    print(divide(10, 0))
except SystemExit:
    pass

print("\nТестирование функции get_element:")
try:
    sample_list = [1, 2, 3]
    print(get_element(sample_list, 1))
    print(get_element(sample_list, 5))
except SystemExit:
    pass
