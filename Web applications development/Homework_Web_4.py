import requests

def get_weather(city_name, api_key):
    base_url = "http://api.openweathermap.org/data/2.5/weather"
    params = {
        "q": city_name,
        "appid": api_key,
        "units": "metric"
    }
    response = requests.get(base_url, params=params)
    if response.status_code == 200:
        return response.json()
    else:
        return {"error": f"Error {response.status_code}: {response.reason}"}

def test_get_weather(monkeypatch):
    def mock_get_successful(*args, **kwargs):
        class MockResponse:
            status_code = 200

            @staticmethod
            def json():
                return {
                    "main": {"temp": 15},
                    "weather": [{"description": "clear sky"}],
                    "name": "Kyiv"
                }
        return MockResponse()
    def mock_get_failed(*args, **kwargs):
        class MockResponse:
            status_code = 404
            reason = "Not Found"

            @staticmethod
            def json():
                return {"error": "Error 404: Not Found"}
        return MockResponse()

    monkeypatch.setattr("requests.get", mock_get_successful)
    response = get_weather("Kyiv", "dummy_api_key")
    assert response["main"]["temp"] == 15
    assert response["weather"][0]["description"] == "clear sky"
    assert response["name"] == "Kyiv"


    monkeypatch.setattr("requests.get", mock_get_failed)
    response = get_weather("UnknownCity", "dummy_api_key")
    assert response["error"] == "Error 404: Not Found"

if __name__ == "__main__":
    from pytest import main
    main()
