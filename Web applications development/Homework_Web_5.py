
import sqlite3

class User:
    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

    @staticmethod
    def initialize_db():
        try:
            connection = sqlite3.connect("users.db")
            cursor = connection.cursor()
            cursor.execute("""
                CREATE TABLE IF NOT EXISTS users (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    username TEXT UNIQUE,
                    password TEXT,
                    email TEXT UNIQUE
                )
            """)
            connection.commit()
            connection.close()
            print("База даних ініціалізована.")
        except sqlite3.Error as e:
            print(f"Помилка ініціалізації бази даних: {e}")

    def register(self):
        try:
            connection = sqlite3.connect("users.db")
            cursor = connection.cursor()
            cursor.execute("""
                INSERT INTO users (username, password, email)
                VALUES (?, ?, ?)
            """, (self.username, self.password, self.email))
            connection.commit()
            connection.close()
            print("Користувач успішно зареєстрований!")
        except sqlite3.IntegrityError as e:
            print(f"Помилка реєстрації: {e}")
        except sqlite3.Error as e:
            print(f"Інша помилка: {e}")

    @staticmethod
    def login(username, password):
        try:
            connection = sqlite3.connect("users.db")
            cursor = connection.cursor()
            cursor.execute("""
                SELECT * FROM users WHERE username = ? AND password = ?
            """, (username, password))
            user = cursor.fetchone()
            connection.close()
            return user is not None
        except sqlite3.Error as e:
            print(f"Помилка входу: {e}")
            return False

User.initialize_db()

while True:
    print("\nОберіть опцію:")
    print("1 - Зареєструватися")
    print("2 - Увійти")
    print("3 - Вийти")

    choice = input("Ваш вибір: ")

    if choice == "1":
        username = input("Введіть ім'я користувача: ")
        password = input("Введіть пароль: ")
        email = input("Введіть email: ")
        user = User(username, password, email)
        user.register()

    elif choice == "2":
        username = input("Введіть ім'я користувача: ")
        password = input("Введіть пароль: ")
        if User.login(username, password):
            print("Успішний вхід!")
        else:
            print("Неправильні дані!")

    elif choice == "3":
        print("До побачення!")
        break

    else:
        print("Невірний вибір. Спробуйте ще раз.")

