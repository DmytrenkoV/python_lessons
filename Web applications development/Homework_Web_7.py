import requests
from bs4 import BeautifulSoup
import sqlite3
def setup_database():
    conn = sqlite3.connect('rozetka_products.db')
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS products (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            price TEXT,
            rating TEXT,
            image_url TEXT
        )
    ''')
    conn.commit()
    return conn

def scrape_rozetka(url):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36"
    }
    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')

    products = []
    items = soup.find_all('div', class_='goods-tile__inner')
    for item in items:
        name_tag = item.find('a', class_='goods-tile__heading')
        name = name_tag.text.strip() if name_tag else 'Название не найдено'

        price_tag = item.find('span', class_='goods-tile__price-value')
        price = price_tag.text.strip() if price_tag else 'Цена не найдена'

        rating_tag = item.find('span', class_='goods-tile__rating')
        rating = rating_tag['aria-label'] if rating_tag else 'Нет рейтинга'

        image_tag = item.find('img', class_='lazy_img')
        image_url = image_tag.get('src', 'Нет изображения') if image_tag else 'Нет изображения'

        products.append({
            'name': name,
            'price': price,
            'rating': rating,
            'image_url': image_url
        })
    return products

def save_to_database(conn, products):
    cursor = conn.cursor()
    for product in products:
        cursor.execute('''
            INSERT INTO products (name, price, rating, image_url)
            VALUES (?, ?, ?, ?)
        ''', (product['name'], product['price'], product['rating'], product['image_url']))
    conn.commit()

if __name__ == '__main__':
    url = 'https://rozetka.com.ua/mobile-phones/c80003/producer=apple/'  # URL для Apple
    conn = setup_database()
    products = scrape_rozetka(url)
    save_to_database(conn, products)
    conn.close()

    print("Данные успешно сохранены в базу данных!")
